import { Account, Currency } from '../graphql';

export const CURRENCY_RUR: Currency = {
    code: 11,
    mnemonicCode: 'RUR',
    minorUnits: 2
};

export const CURRENCY_USD: Currency = {
    code: 22,
    mnemonicCode: 'USD',
    minorUnits: 2
};

export const CURRENCY_EUR: Currency = {
    code: 33,
    mnemonicCode: 'EUR',
    minorUnits: 100
};

export const ACCOUNTS: Account[] = [
    {
        number: '13123123123123600681',
        balance: 12345677,
        currency: CURRENCY_EUR,
        cards: []
    },
    {
        number: '13123123123123600682',
        balance: 883562301,
        currency: CURRENCY_RUR,
        cards: [
            {id: '90099090', number: '1'},
            {id: '90099091', number: '2'}
        ]
    },
    {
        number: '13123123123123600683',
        balance: 0,
        currency: CURRENCY_USD,
        cards: []
    }
];
