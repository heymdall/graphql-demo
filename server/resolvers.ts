import { ACCOUNTS } from './data';
import { Account, AccountQueryArgs, AccountMutationResponse } from '../graphql';

function timeout(time: number, data: any): Promise<any> {
    return new Promise(resolve => {
        setTimeout(() => resolve(data), time);
    })
}

export const Query = {
    accounts(): Promise<Account[]> {
        return timeout(400, ACCOUNTS);
    },
    account(_, query: AccountQueryArgs): Promise<Account> {
        return timeout(400, ACCOUNTS.find(a => a.number === query.number));
    }
};

interface WithdrawQuery {
    number: string;
    amount: number;
}

export const Mutation = {
    withdrawAccount(_, query: WithdrawQuery): Promise<AccountMutationResponse> {
        let account = ACCOUNTS.find(a => a.number === query.number);
        const errors: string[] = [];
        if (account.balance - query.amount < 0) {
            errors.push('amount', 'Not enough money!');
        } else {
            account.balance = account.balance - query.amount;
        }

        return timeout(400, {
            account,
            errors
        });
    }
};
