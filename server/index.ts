import * as express from 'express';
import * as bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
import { makeExecutableSchema, addMockFunctionsToSchema } from 'graphql-tools';
import { readFileSync } from 'fs';
import * as path from 'path';
import { Query, Mutation } from './resolvers';
import * as cors from 'cors';

const typeDefs = readFileSync(path.resolve(__dirname, '../graphql/schema.gql'), 'utf8');

const resolvers = {
    Query,
    Mutation
};

const schema = makeExecutableSchema({ typeDefs, resolvers });
// addMockFunctionsToSchema({ schema });
const app = express();
app.use(cors());
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));
app.listen(4000, () => console.log('Now browse to localhost:4000/graphiql'));
