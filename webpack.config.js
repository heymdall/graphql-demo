const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MQ = require('arui-feather/src/mq/mq.json');

module.exports = {
    entry: ['./client/index.jsx'],
    resolve: {
        root: [
            path.join(__dirname, 'node_modules'),
        ],
        extensions: ['', '.js', '.jsx', '.ts', '.tsx'],
    },
    output: {
        path: path.join(__dirname, './build'),
        filename: 'index.js'
    },
    module: {
        loaders: [
            {
                test: /\.json?$/,
                loader: 'json-loader',
            },
            {
                test: /\.tsx?$/,
                loader: 'babel!ts-loader?configFileName=client/tsconfig.json',
            },
            {
                test: /\.jsx?$/,
                loader: 'babel',
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader?sourceMap!postcss-loader',
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?mimetype=application/font-woff',
            },
            {
                test: /\.(ttf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?mimetype=application/octet-stream',
            },
            {
                test: /\.(eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader',
            },
            {
                test: /\.(jpe?g)$/i,
                loader: 'url-loader?mimetype=image/jpeg'
            },
            {
                test: /\.(png)$/i,
                loader: 'url-loader?mimetype=image/png'
            },
            {
                test: /\.(gif)$/i,
                loader: 'url-loader?mimetype=image/gif'
            },
            {
                test: /\.(svg)$/i,
                loader: 'url-loader?mimetype=image/svg+xml'
            },
        ],
    },
    postcss: function(webpack) {
        return [
            require('postcss-import')({
                addDependencyTo: webpack,
            }),
            require('postcss-url')({
                url: 'rebase',
            }),
            require('postcss-mixins'),
            require('postcss-for'),
            require('postcss-each'),
            require('postcss-simple-vars')({
                variables: {
                    gridMaxWidth: '1000px',
                    gridGutter: '10px',
                    gridFlex: 'flex',
                },
            }),
            require('postcss-custom-media')({
                extensions: MQ,
            }),
            require('postcss-custom-properties'),
            require('postcss-calc'),
            require('postcss-nested'),
            require('autoprefixer')({
                browsers: [
                    'last 2 versions',
                    'ie >= 9',
                    'android >= 4',
                    'ios >= 8'
                ],
            }),
        ];
    },
    plugins: [
        new webpack.ProvidePlugin({
            React: 'react',
        }),
        new HtmlWebpackPlugin({
            hash: true,
            template: './client/index.html.ejs'
        }),
        new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(ru|en-gb)$/),
    ]
};
