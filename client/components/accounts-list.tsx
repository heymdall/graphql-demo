import { Component } from 'react';
import gql from 'graphql-tag';
import { graphql, InjectedGraphQLProps } from 'react-apollo';
import { Account as AccountModel } from '../../graphql';
import Spin from 'arui-feather/src/spin/spin';
import Account from './account';

const MyQuery = gql`query MyQuery { accounts { number balance currency { mnemonicCode minorUnits } } }`;

export interface AccountsListProps extends InjectedGraphQLProps<{ accounts: AccountModel[] }> {
}

@graphql(MyQuery)
export default class AccountsList extends Component<AccountsListProps, {}> {
    render() {
        return <div>
            <div style={ {textAlign: 'center'} }>
                <Spin visible={ this.props.data.loading }/>
            </div>

            <div style={ {display: 'flex', justifyContent: 'space-between'} }>
                { !this.props.data.loading &&
                    this.props.data.accounts.map(item => <Account key={ item.number } {...item} />)
                }
            </div>
        </div>
    }
}
