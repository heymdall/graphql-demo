import { Component } from 'react';
import gql from 'graphql-tag';
import MoneyInput from 'arui-feather/src/money-input/money-input';
import Button from 'arui-feather/src/button/button';
import Spin from 'arui-feather/src/spin/spin';
import Icon from 'arui-feather/src/icon/icon';
import { graphql } from 'react-apollo'; import { parseErrors } from '../lib/parse-errors';
require('./charger.css');

interface ChargerProps {
    number: string;
    minority: number;
    withdraw?: (number: string, amount: number) => Promise<any>;
}
interface ChargerState {
    amount: number;
    isLoading: boolean;
    error?: string;
}

const withdrawAccount = gql`
    mutation withdrawAccount($number: String!, $amount: Int!) {
        withdrawAccount(number: $number, amount: $amount) {
            account {number balance}
            errors
        }
    }
`;

@graphql(withdrawAccount, {
    props: ({ mutate }) => ({
        withdraw: (number: string, amount: number) => mutate({ variables: { number, amount } }),
    }),
})
export default class Charger extends Component<ChargerProps, ChargerState> {
    state = {
        amount: 100,
        isLoading: false,
        error: null
    };

    handleButtonClick = () => {
        this.setState({
            isLoading: true
        });

        this.props
            .withdraw(this.props.number, this.state.amount * this.props.minority)
            .then(({ data }) => {
                const newState: any = { isLoading: false };
                const parsed = parseErrors(data.withdrawAccount.errors);
                if (parsed.fieldErrors['amount']) {
                    newState.error = parsed.fieldErrors['amount'];
                }
                this.setState(newState)
            });
    };

    handleAmountChange = (value: string) => {
        this.setState({
            amount: parseInt(value, 10) || 0,
            error: null
        });
    };

    render() {
        return (
            <div style={ {paddingRight: 10} }>
                <MoneyInput
                    value={ this.state.amount.toString() }
                    onChange={ this.handleAmountChange }
                    error={ this.state.error }
                    rightAddons={
                        <Button
                            className='charger__button'
                            onClick={ this.handleButtonClick }
                            text={ this.state.isLoading
                                ? <Spin visible={ true } className='charger__spin' />
                                : <Icon action='ok' />
                            }
                        />
                    }
                />
            </div>
        )
    }
}
