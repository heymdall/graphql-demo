import { Component } from 'react';
import Label from 'arui-feather/label';
import FormField from 'arui-feather/form-field';
import Amount from 'arui-feather/amount';
import CardNumber from 'arui-feather/card-number'

import Charger from './charger';
import { Account as AccountModel } from '../../graphql';

export default class Account extends Component<AccountModel, {}> {
    render() {
        const amount = {
            value: this.props.balance,
            currency: {
                code: this.props.currency.mnemonicCode,
                minority: this.props.currency.minorUnits
            }
        };
        return (
            <div>
                <FormField label={ <Label>Номер:</Label> }>
                    <CardNumber value={ this.props.number } />
                </FormField>
                <FormField label={ <Label>Баланс:</Label> }>
                    <Amount amount={ amount } />
                </FormField>
                <FormField>
                    <Charger number={ this.props.number } minority={ this.props.currency.minorUnits }/>
                </FormField>
            </div>
        );
    }
}
