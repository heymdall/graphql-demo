import { Component } from 'react';
import ThemeProvider from 'arui-feather/src/theme-provider/theme-provider';
import Page from 'arui-feather/src/page/page';
import Header from 'arui-feather/src/header/header';
import Footer from 'arui-feather/src/footer/footer';
import AppTitle from 'arui-feather/src/app-title/app-title';
import Heading from 'arui-feather/src/heading/heading';
import AppMenu from 'arui-feather/src/app-menu/app-menu';
import Menu from 'arui-feather/src/menu/menu';
import AppContent from 'arui-feather/src/app-content/app-content';
import AccountsList from './accounts-list';


export default class App extends Component<{}, {}> {
    render() {
        return (
            <ThemeProvider theme='alfa-on-color'>
                <Page header={ <Header /> } footer={ <Footer /> }>
                    <AppTitle>
                        <Heading>GraphQL</Heading>
                    </AppTitle>
                    <AppMenu>
                        <Menu />
                    </AppMenu>
                    <AppContent>
                        <AccountsList/>
                    </AppContent>
                </Page>
            </ThemeProvider>
        );
    }
}
