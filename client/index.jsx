import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import ApolloClient, { createNetworkInterface } from 'apollo-client';
import 'arui-feather/main.css';
import App from './components/app.tsx';

const client = new ApolloClient({
    networkInterface: createNetworkInterface({ uri: 'http://localhost:4000/graphql' }),
    dataIdFromObject: (result) => {
        let idField = 'id';
        switch (result.__typename) {
            case 'Account':
                idField = 'number';
        }
        if (result[idField] && result.__typename) {
            return result.__typename + result[idField];
        }
        return null;
    }
});

const appElement = document.getElementById('root');

const renderApp = (App) => {
    ReactDOM.render(
        <ApolloProvider client={ client }>
            <App />
        </ApolloProvider>,
        appElement
    );
};

renderApp(App);

if (module.hot) {
    module.hot.accept('./components/app', () => {
        renderApp(require('./components/app.tsx').default);
    });
}
