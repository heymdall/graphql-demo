export interface ParsedErrors {
    globalErrors: string[];
    fieldErrors: { [key: string]: string; };
}
export function parseErrors(serverErrors: string[]): ParsedErrors {
    const errors: ParsedErrors = {
        globalErrors: [],
        fieldErrors: {}
    };

    for (let i = 0; i < serverErrors.length; i += 2) {
        let field = serverErrors[i];
        let description = serverErrors[i + 1];
        if (field) {
            errors.fieldErrors[field] = description;
        } else {
            errors.globalErrors.push(description);
        }
    }

    return errors;
}
