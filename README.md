# Простое демо для graphql

Из-за того, что наш артифактори не поддерживает установку scoped пакетов
с установкой надо чуток повозится.

1. Убрать arui-feather из зависимостей.
2. Установить все `npm i`, используя стандартный npm registry
3. Установить arui
```
npm config set registry http://mvn/artifactory/api/npm/npm
npm i arui-feather
```

# Запуск

```
npm start
```

Graphql сервер будет доступен по адресу `localhost:4000`

* [localhost:4000/graphql](localhost:4000/graphql) - эндпоинт для запросов
* [localhost:4000/graphiql](localhost:4000/graphiql) - графический интерфейс graphql

Реактовое приложение будет доступно по адресу [localhost:3003/](localhost:3003/)

Для генерации typescript моделей из gql описаний используйте `npm run generate`