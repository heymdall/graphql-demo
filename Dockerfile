FROM mhart/alpine-node:6.9.1

RUN mkdir -p /usr/src/app/server
RUN mkdir -p /usr/src/app/qgraphql
WORKDIR /usr/src/app
RUN npm install yarn -g

COPY .babelrc /usr/src/app
COPY package.json yarn.lock tsconfig.json /usr/src/app/

RUN apk add --no-cache --virtual .build-deps make gcc g++ python && \
    yarn install && \
    yarn cache clean && \
    apk del .build-deps

EXPOSE 80

CMD [ "npm", "run", "start:server" ]
