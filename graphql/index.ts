
export interface Query {
  accounts: Array<Account>;
  account: Account;
}

export interface AccountQueryArgs {
  number: string;
}

export interface Account {
  number: string;
  balance: number;
  currency: Currency;
  cards: Array<Card> | null;
}

export interface Currency {
  code: number | null;
  mnemonicCode: string;
  minorUnits: number | null;
}

export interface Card {
  id: string;
  number: string;
}

export interface Mutation {
  withdrawAccount: AccountMutationResponse;
}

export interface WithdrawAccountMutationArgs {
  number: string;
  amount: number;
}

export interface AccountMutationResponse {
  account: Account;
  errors: Array<string>;
}
